# Cybersecurity

## Resources

Research some recent difficult bugs and see if you can reproduce them.
Learn about git bisect to discover the exact line of code in the source code that created the bug or vulnerability that you were able to reproduce.

- opensourcesecurity.io blog post on the 2021 [log4j bug](https://opensourcesecurity.io/2021/12/12/log4j-is-hard-to-find-and-harder-to-fix/) by [Kurt](https://twitter.com/kurtseifried) and [Josh](https://twitter.com/joshbressers)
- opensourcesecurity.io [podcast #314](https://opensourcesecuritypodcast.libsyn.com/episode-314-the-linux-dirty-pipe-vulnerability) by [Kurt](https://twitter.com/kurtseifried) and [Josh](https://twitter.com/joshbressers)
- [Dirty Pipe Bug writeup](https://dirtypipe.cm4all.com/) by Max Kellermann (max.kellermann@ionos.com) on his [cm4all blog](https://www.cm4all.com/en/blog)
