# Assignment: Build a Full Text Search Engine
​
## References

- [MeiliSearch: A Minimalist Full-Text Search Engine by Mark Litwintschik](https://tech.marksblogg.com/meilisearch-full-text-search.html)
- [Build a Search Tool using NLP](https://www.manning.com/bundles/search-tool-with-nlp-ser?utm_source=linkedin&utm_medium=organic&utm_campaign=liveproject_bondarenko_build_7_28_21)

MeiliSearch's Source Code

## Complexity Comparison

* [MeiliSearch](https://github.com/meilisearch/MeiliSearch): 8K lines of Rust
* [Elasticsearch](https://www.elastic.co/): 2M lines of Java
* [Apache Solr](https://solr.apache.org/) ([Apache Lucene](https://lucene.apache.org/)): 1.3M lines of Java
* [Groonga](https://groonga.org/): 6K lines of C
* [Manticore Search](https://manticoresearch.com/): 150K lines of C++
* [Sphinx](http://sphinxsearch.com/): 100K lines of C++
* [Typesense](https://typesense.org/): 50K lines of C++
* [Tantivy](https://github.com/tantivy-search/tantivy): 40K lines of Rust
* [SRCHX](https://github.com/alash3al/srchx) ([Bleve](https://github.com/blevesearch/bleve) web app): 83K lines of GoLang


## Setting up Meili on Digital Ocean

Getting your tokens: https://docs.meilisearch.com/reference/features/authentication.html#adding-the-master-key
Getting 41,242 articles from Wikipedia: `wget https://dumps.wikimedia.org/enwiki/latest/enwiki-latest-pages-articles1.xml-p1p41242.bz2` (250MB)
Getting all (4,045,402) English articles from Wikipedia dump: `wget enwiki-latest-pages-articles.xml.bz2` (19GB)
