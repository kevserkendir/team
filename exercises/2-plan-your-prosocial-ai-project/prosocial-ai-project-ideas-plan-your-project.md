# Prosocial Machine Learning

## Python and Computer Science

- **Advent of Code** puzzles
    - Read up on [adventofcode.com/2021](https://adventofcode.com/2021/about) to see if you're interested
    - Sign up for an account using Reddit or Twitter (never use BigTech Google or GitHub Auth)
    - Go to [Leaderboard], then [Private Leaderboard], join San Diego Python’s leaderboard (`149367-c3d37740`).
    - Click around to figure out how to navigate to the first problem on Day 1, search the HTML source with a debugger if you like.
    - If you didn't find the Day 1 problem you can try de-obfuscating this URL [`https colon FQDN slash 2021 slash day slash 1`](https://adventofcode.com/2021/day/1) (click on the link to cheat)
- **HTML Compiler & Parser**
    - Write a script to convert from markdown to HTML
    - Write another script to convert from an HTML template to rendered HTML
    - Write another script to convert from an HTML template to HTML
    - Write another script to render an HTML template and convert to docx
    - Post a link to your solution (gitlab repo) on Stack Overflow: https://stackoverflow.com/a/70408156/623735
    - Here's a way to do it with pandoc: https://www.mscharhag.com/software-development/pandoc-markdown-to-pdf

## ML
- **hypertable**
    - save/load to yaml and csv
    - Pipeline element class that does model evaluation automatically
    - display as dataframe.sort_values
- **automl**
    - read first 10 lines of CSVs in a directory
    - guess features types
    - create data-dictionary.yml
    - df and load_csv decorator for pipline functions
    - .fit/.transform/.predict/.__init__ (Pipeline element class)
    - proai.DataFrame() that handles sparse
    - tqdm-like wrapper for .fit that converts back to sparse or dense dataframe
- **daxiv (data arxiv.org)**
    - django-api app
    - admin interface
    - table view of datasets
    - normalize db of datasets by tables rather than collections
    - get all past interns to upload their datasets
    - store on public digital-ocean spaces urls
- **synthetic data mystery**
    - function to generate a random mathematical polynomial
    - function to generate random "traffic" of sequential datetime stamps
    - function to generate random words

## NLP

- **nessvec composer** (Beginner)
    - interactively compose a nessvector
    - have qary save and load the nessvector model for future word research
- **nessvec perfect word** (Intermediate)
    - chatbot to help you find the perfect word 
    - or the person/word on the tip of your tongue
- **gratitude assistant** (Beginner)
    - chatbot to help you compose a greeting card or thank you note
    - ask questions about the other person like significard
    - give code to significard.com or other small businesses 
- **bureaucracy bot** (Expert)
    - like traffic ticket chatbot in NYC and Boston
    - help people file LLC and nonprofit paperwork with State Department
    - provide FAQ answers
    - give lay-person lawyers a way to provide feedback
    - train an ML generative model to avoid legal challenges or "lawyer referral service" regulation
- **unbreakable urls** (Advanced)
    - search git logs for moved files/dirs
    - update URLs in markdown
    - merge request system to allow coder to review changes
    - dataset of all fixes rejected by user/coder 
    - update URLs in code?
    - update URLs in HTML
    - crawler to find broken links in a repo
    - use nlp (nessvec) to detect file moves
    - spellchecker for URLs
- **book visualizer**: Summarize the sections of a book (Advanced)
    - 0: asciidoc parser to create python data structure: [asciidoc3](https://gitlab.com/asciidoc3/asciidoc3)
    - 1: find a pretrained long document summarizer:
        - [Arman Cohen's point-generator network (tf)](https://github.com/armancohan/long-summarization)
        - [Wendy Xiao's global+local extractive summarizer](https://github.com/Wendy-Xiao/Extsumm_local_global_context)
        - [Andrei Mircea's BERT-based extractive summarizer](https://github.com/mirandrom/HipoRank)
    - 2: run summarizer on the NLPIA book, 3 parts, 14 chapters, sections, paragraphs
    - 3: tree structure with headings, like TOC by expandable
- **Better Copilot (Codex)** (Advanced)
    - research parsers/compilers for python:
    - faq knowledge base with pattern and antipattern info similar to 'qary/data/faq/faq-python-programming...yml'
    - research programming patterns and antipatterns
    - import Rosetta code, Wikipedia, free online code books, and other authoritative algorithm and code examples
    - generalize/abstract from comments and code snippets in someone's code during linter running
    - match abstractions of code (functions) to code snippets from reputable sources
    - build a better Codex that actually assists you in writing code, using antipattern and pattern knowledge
- **graph game** (Advanced)
    - fork, clone, install proai_playground_backend and _frontend
    - create a svelte component that displays an interactive network graph
    - reveal nodes at frontier as each node is clicked on (but not edge cost)
    - player must navigate the graph optimally for typical graph algorithm objectives (shortest path, spanning tree)
    - can be a word guessing game based on a markhov model like semandle
    - sentence generation game
    - password guessing game
    - idea: provide graph embedding or other embedding or heuristic data
    - advanced: allow user to write heuristic expression in js
- **graph flow visualization** (Advanced)
    - check out day in the life of an american [example](https://flowingdata.com/2015/12/15/a-day-in-the-life-of-americans/) (near bottom of page)
    - find dataset of conversations with a chatbot - nodes are states or sentiment of bot statements, flowing balls/points are individual users as they progress through a conversation
    - Sentences in a section of NLPiA can be the pingpong balls, nodes can be the sentiment of those sections/paragraphs clustered into num_chapters+3 topics?
- **assembly theory** (Advanced)
    - [Lex interview of L](https://www.youtube.com/watch?v=ZecQ64l-gKM)
    - Follow [Lee Cronin](https://www.chem.gla.ac.uk/cronin/)
- **dialog graph algorithms** (Advanced)
    - skim the [CS224w schedule](http://web.stanford.edu/class/cs224w/index.html#schedule) for graph similarity algorithms
    - research draw.io file format api in python (try to write a draw.io file for a 2-layer tree with one root node and 3 leaf nodes)
    - research python graph data structures or databases:
        - PyG (PyTorch Geometric)
        - SNAP.PY
        - NetworkX
        - AIMA book and python source code
        - dict of dicts
        - graphviz
        - draw.io
    - function to create graph data structure (compatible with above research) from qary `*.v2.dialog.yaml` files
    - build minimum spanning dialog tree for a qary `*.v2.dialog.yaml` files
    - search minimum spanning dialog tree for minimum paths to each edge
    - measure sequence similarity between each of the minimum dialog tree paths
- **taste game**
    - build cli or web game where people taste chocolate, tea, coffee and write a description of their tasting notes
    - create NL embedding of player tasting notes and compare to profession tasting notes or advertising copy for style, positivity, and semantic accuracy
    - show statistics on how much "fluff" and nonsense there is in tasting notes and ad copy
- **open source dynabench**
    - install [label studio](https://labelstud.io/guide/)
    - use some example templates for NLP to label some of your own text
    - play with dynabench.org (from facebook research)
    - see if it's possible for labelstudio to have a new text input field like in dynabench
    - try to recreate one of the simpler dynabench examples in label studio
    - publish your label studio config file on their website and in gitlab
- **book club facilitator**
    - expand on template-based question generator:
    - add author-specific q based on author gender or orientation
    - add author-specific q based on author age
    - add author age predictor (based on book title trigrams, name trigrams, book summary)
    - add character-specific q based on gender
    - add character-specific q based on age
    - add conclusion/theme/tone-specific q based on summary
    - add genre-specific questions
    - add summary-specific questions
    - train template selector ML model (one binary target for each template)
    - train question predictor (based on record for each template embedding for each template)
    - train information extractor ML model
- **text generator**
    - autoencoder
    - markhov chain (from previous work)
- **conversation datasets**
    - retrieve facebook research convo datasets
    - normalize and unify schema for all convo datasets
    - django models for convo schema
- **friendly lurker**
    - build a background keylogger [with Python](https://www.geeksforgeeks.org/design-a-keylogger-in-python/#keylogger-in-inux): focus on Linux
    - use NLP on what the user types and popup windows with suggest help
        1. spelling
        2. grammar
        3. style
        4. knowledge/search
        5. fact-check
        6. code completion
        7. SO answers
        8. philosophy
        9. productivity
        10. mental health
        11. pomodoro breaks
    - build a terminal keytyper (might be able to log into shared tmux?)
    - build a browser keytyper (FF plugin or selenium)
    - build a linux keytyper (lurks all active GUI windows with text entry UIs)
    -

## qary

- **qary serve** (Advanced)
    - Django Rest Framework app (see gitlab.com/tangibleai/playground_svelte_backend)
    - use django `python manage.py runserver`
- **qary subcommands** (Advanced)
    - use [Cliff](https://docs.openstack.org/cliff/latest/) to create subcommands for each skill
    - use [Cliff](https://docs.openstack.org/cliff/latest/) to create subcommands for common **combinations of skills** & configuration options
- **qary smarter NLU** (Moderate)
    - Intent() recognizer in quiz.py
    - BERT embedding from sentence-transformer
    - test.yml for kalika FUZZY
    - test.yml for BERT
- **qary init** (Advanced)
    - template skill.py
- **qary typeform for internship** (Advanced)
    - send answers to django-api
- **qary-docker** (Moderate)
    - add pgdb
    - add rabbitmq or reddis or mongo
- **qary-terraform** (Moderate)
    - add pgdb
    - add rabbitmq or reddis or mongo
- **qary graphviz** (Moderate)
    - functions to create visualizations of qary `*.v2.dialog.yaml` files.

### NLPiA

- **ch 8 author helper** find a place for a new concept using BERT sentence embeddigns
    - parse adoc in to xml
    - parse xml into json
    - parse json paragraphs with space to get sentences
    - (first sentecnce and heading of each section? or just one chapter)
    - sentence embeddings with sentence transformers
    - index sentence vectors with annoy
    - find closest sentences in book
- **ch 5 markov chain**
- **ch 3 FAQ chatbot**
- **ch 4 Fake news**

### Web

- **ideabase**: These project ideas in a webapp
    - Convert this markdown file to yaml using regexes or NLP
    - Create a database schema in Django based on the YAML file
    - Create a Django App that interacts with the ideas in sqlite DB
    - Enable the admin interface so admin users can add and edit projects
    - Create `Profile(User)` models
    - Create Sign-Up page
    - Create Login page
    - Allow signed-in users to edit projects they created
    - Allow signed-in users to add new project ideas
- [ ] **data.qary.ai** based on django-api + onow upload button
- [x] **svelte-django CORS**
- [x] **svelte todo app** (online example)
- [ ] **svelte todo app db** (online example)
- **streamlit app for qary**
- **dash app for qary analytics**
- **svelte chat app** (based on todo exmample)

### Conversation Design

- **covid lockdown coach**
    - 0: improve skills/life.py
    - 1: help people deal with financial and health stress of covid lockdown/isolation
- **dynoscore**
    - 1: review Christopher Potts lecture on social responsibility and computational sociolinguistics:  https://youtu.be/t_A36DDcG_0
- **author n-gram helper**
    - 1: download the 3-grams from Google's n-gram viewer data dump
    - 2: filter out the 3-grams that include non scrabble dictionary words (no proper nouns, no punctuation except apostrophe)
    - 3: create a tokenizer that's equivalent to "how we process books", e.g. replaced "can't" with "can not".
    - 4: create 3-level dict or some other index for the 3-grams
    - 5: split input text into phrases (delimitted by commas, quotes, semicolons, periods, question marks, etc.)
    - 6: apply same tokenizer as in 3 to input text phrases that user wants to check
    - 7: flag unlikely 3-grams for the user
    - 8: suggest better 3rd gram from 3-gram database, using synonyms and first two layers/grams of 3-gram
- **Ally bot**
    - 1: read this [blog post](https://juliepagano.com/blog/2014/05/10/so-you-want-to-be-an-ally/) by Julie Pagano
    - 2: think about a conversation design to nudge discriminatory/trolly conversations into a more productive direction
    - 3: add gentle "nudges" from Julie Pagano and even advanced influence techniques from _Impossible Conversations_ by Peter Boghossian
    - 4: create a qary chatbot skill to implement `ally.py`
    - 5: add a reddit or twitter channel to qary so your ally can have an impact in public conversations
- **happy maker**
    - 0: copy existing `skills/quiz.py` and the `.yaml` file that creates the quiz
    - 1: implment the burns emotion log/journal (4 questions)
    - 2: load history of previous answers to questions
    - 3: display some numbers or trends or statistics about the user's emotion journal
    - 4: ask questions to improve emotion journal
    - 5: suggest ideas to improve emotional health
- **language IQ**
    - 1: create list of smart people with URLs to resources about them: mind-sports winners, kaggle competition winners, nobel prize winners: https://en.wikipedia.org/wiki/List_of_world_championships_in_mind_sports
    - 2: create corpora of content written about people listed: wikipedia, news, biographies
    - 3: create corpora of content written by people listed: twitter, papers, books, youtube transcripts
    - 4: normalization approach so that tweets are scored differently than books
    - 5: create "negative" test set from random sample of wikipedia biographies as "seed"
    - 6: build algorithm that adds more smart people to the list and removes smart people from the negative list and adds more less smart people from the negative list
    - 7: compare to Flesche-Kincaid reading level score
- **language supercooperativeness score**
    - 1: trolls and sociopath lists
    - 2: supercooperator list (nobel peace prize, social impact people, examples from supercooperator book)
    steps 2-6 from language IQ project
- **creative problem solving coach**
    - 0: ideas here: https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/chatbots/creative-problem-solving-coach.md
    - 1: encode several meta cognition tips as flows or conversation threads in the quiz skill
- **conversation-right** - sentiment analysis to detect conversation styles
    - 1: listen to "why conversations go wrong" podcast
    - 2: research "How to Have Impossible Conversations" (contains several dialog transcripts)
- **Rasa Chatbot end-to-end** - build a RASA chatbot based on your own idea, or based on one of Tangible AI's existing projects:
    - 1: create a simple "hello, world" chatbot on RASA and deploy it to the web
    - 2: connect the chatbot to channel of your choice (Web interface, Telegram, etc.)
    - 3: learn to use quick replies, RASA forms for deterministic dialog
    - 4: implement FAQ and experiment with RASA's NLU model to see if the results change
    - 5: (variation): build a multi-lingual RASA chatbot - use fasttext embeddings
- **Syndee** emotional coaching ability
    - 1: Review Burns' "How to Be Happy"
    - 2: structure the dialog content here: https://gitlab.com/tangibleai/qary/-/blob/master/src/qary/data/life_coach/
- **Syndee new functionality** - add conversations and dialogues into Syndee, our flagship chatbot that helps users cope with impostor syndrome.
    - 1: get familiar with Landbot platform and add small changes to existing flows
    - 2: learn to use Amplitude for product analytics and add new analytics event to Syndee
    - 3: design a new coaching/therapeutic conversation to help the user cope with Impostor Syndrome.
    - 4: incorporate the tool inside Syndee
- **sexual education for teens in Tanzania** - create a prototype of a bot to help Tai Tanzania educate youth in Tanzania on sexual education topics
    - 1: get familiar with the project and the organization
    - 2: design the chatbot concept and structure
    - 3: architect the chatbot tech stack
    - 4: build a prototype to test with users
    - 5: improve the bot after user feedback
- **history tutor** - Vish working on it
- **math tutor** - talk to Billy for ideas

## NLP

- **diverse news sources for qary pulse skill**
    - https://changelog.com/podcasts
    - https://learnawesome.org
    - https://substack.org
    - https://stackoverflow.com
    - https://stackexchange.com
    - https://ask.com ?
    - Telegram forums ?
- **Priority SMS Inbox for Nonprofits**
    - listen to this: https://changelog.com/practicalai/145
    - NLP: train a model to recognize (triage) priority healthcare SMS messages (depression, childbirth, sick child, first aid)
    - webapp: API (Django REST Framework) to help nonprofits prioritize their messaging inbox
- **Transliteration**
    - function to translitarate from Burmese script to latin characters ascii? (see Maria)
- **AutoML foundation**
    - data type detector for Automatic ML
- **semantic parser**
    - https://github.com/tangibleai/sempre
- **kindness amplifier**
    - Goal: translator from unkind, insensitive, discriminatory statements into statements more mindful of the variety of human needs and their value as individuals
    - find lists of politically correct statements
    -
- **wiki quest 1.0**
    - use qary logs to create questions that can be answered by qary on Wikipedia
    - build text adventure game where points are achieved by speed at finding facts that answer the question
    - add questions that are controversial/scientific that require avoidance of distractors/fakenews/clickbait
    - add score for accuracy (based on semantic embedding?)
- advanced: **wiki quest 2.0**
    - build a wikipedia browser that uses python `requests` package and renders the page locally
    - present the user a random wikipedia page and give them a destination page topic or title
    - user tries to find that page clicking on links within the random page
    - intercept all links so that they can be filtered to increase difficulty (score based on difficulty of challenge)
        - lists pages
        - categories pages
        - disambiguation pages
    - build graph of wikipedia crosslinks and present only solvable problem
    - build AI that can accomplish the task using NLP and A* search, etc.
    - use the AI to do research on the web crawling to find what you're looking for

- **book club facilitator AI**
    - Something like https://www.ourbookworm.com/ using Martha's book summary question generator or Aditi's SQUAD-based question generator
    - Ping marth Gavinda about a book club forum converwsation dataset you could use to fine tune GPT-J. The prompt should be a summary of the plot of the book.
- **Compare tokenizers**
    - Compare [BERT tokenizer](https://huggingface.co/transformers/model_doc/bert.html#berttokenizer) and BPE tokenizer to spacy and other tokenizers on speed, and supervised learning problems using XGBoost vs LogisticRegression vs nn.Linear() layer. GPT-2 byte-level tokenizer too.
- **GPT-J transfer learning**
    - Compare various embeddings from hugging face sentence-transfomers (BERT, distilBERT) on a benchmark problem like fake news detection or sentiment analysis or Winnograd benchmarks:
    - Create embedding pipeline for GPT-2 and GPT-J and compare results on those same benchmarks: https://github.com/huggingface/transformers/issues/3168#issuecomment-596354816
- **one armed bandit**
    - 1: Imagine you are designing a slot machine by generating a sequence of numbers that will be the payout at each pull of the handle (with a minimum payoff of 0 dollars, and a "deposit" of $1 with each pull of the handle). Create a dataset of 1M payoff number samples from sequence of Gaussian distributions switching between 3-5 distributions at only 5 discrete, random moments within the dataset. The parameters of the distributions (std, mean, and 5 "switching moments") must be randomly selected and you must not be able to ever observe the statistics of any of the samples directly or know their exact statistics.
    - 2: create an AI or ML algorithm to optimally decided when to start depositing $1 into the slot machine and pulling the lever vs stepping away vs letting someone else pull the lever
- **reproduce results of an NLP paper**
    - 1: read one of these [summaries of 10 important NLP papers](https://www.topbots.com/nlp-research-papers-2020/#nlp-paper-2020-1)
    - 2: pick one of them and download the benchmark dataset and any pretrained models for the paper
    - 3: run inference (prediction) on the benchmark dataset to confirm the results reported in the paper
    - 4: modify the benchmark test set in some way that would not trick a human, but might reduce the accuracy of the model on your new dataset. Do not change the examples except to add typos, character or word edits/changes that would not cause a human to have any reduction in accuracy at the task/problem.
    - 5: estimate the robustness of the algorithm by estimating the minimum accuracy that the model might achieve if you continued to search for and find all the best adversarial modifications to the test set.
- **prosocial score**
    - 1: load & EDA scored data from intern applications (questionaires)
    - 2: identify which questions are most useful for each score
    - 3: train a TFIDF Ridge/Lasso regressor model to predict applicant ratings
    - 4: train a Spacy doc vector regressor model to predict applicant ratings
    - 5: train a sentence-transformers BERT vector regressor model to predict applicant ratings
    - 6: input additional examples and edge cases to try to "trick" your NLP
- **robust nlp** - incrementally improve robustness of an NLP pipeline
    - 1: train a question answerer, sentiment analyzer, or intent classifier
    - 2: add robustness score based on character perterbatsion for common typos
    - 3: add synonyms to robustness score pipeline
    - 4: add common misspellings to robustness score
    - 5: add acronyms, abbreviations
    - 6: add paraphraser/summarizer
- **BPE analysis**: compaire byte pair encoding to n-gram tokenization and case-folding, etc
    - 1: train conventional LSTM model on some common task like translation w/wo case folding, stop words, NLTK tokenizer
    - 2: plot of accuracy vs training time vs learned parameters speed
    - 3: example tokenizations to discover reasons why one better than another
    - 4: expand vocabulary by training BPE tokenizer on Wikipedia
    - 5: transfer learning to translation problem to compare again
- **single-document question answering** - use qary's qa skill to achieve high accuracy of open question answering on a single document (i.e. wikipedia article) (see demo here: https://qary-single-document-demo.bubbleapps.io/)
    - 1: adjust the qa skill to accept a string to find the answer in
    - 2: create a data testset of questions and answers for 1-2 documents/wikipedia pages
    - 3: benchmark qary's accuracy on the testset and improve it
- **quantified mind**: mind log, consciousness log, quantified brain (self)
    - 1: django form with automatic time tagging of text entry
    - 2: natural language statement of event, categorical for location, numerical for num witnesses, binary for target vs feature (internal sensation vs external event/treatment)
    - 3: autocoding/autocomplete like in doctors office, coding for common events like: take vitamin, eat oatmeal with nuts and fruit, cranberries, pollen, sneeze, cough, sadness, me time, meditate, yoga, pessimism, anxiety, cognitive distortions, happiness, joy, moment of well-being, insight, idea
    - 4: label text not yet autocoded, by clustering words/cocepts with existing autocodes for info extraction (categoricals, tags, clustering of events)
    - 5: train model
    - 6: create web form in django that accomidates basic fields like context, location, time-since event (recorded after the fact by 10 min, 15 min)
    - 7: machine learn the defaults for an individual when they start typing in the description field, user edits the coding that is disconnected from autocomplete once beyond it in desc
    - 8: svelte app (mobile, reactive)
    - 9: understand and deal with CORS (cross-origin request security) [for your `fetch()`](https://javascript.info/fetch-crossorigin_)
    - 9: qary skill
    - 10: mobile app for qary skill
- **good listener**: qary skill to record user feedback and provide encouragement
    - 1: classifier to indicate whether feedback is on most recent chat
    - 2: regressor to score the quality/helpfulness/sentiment of feedback +1/+100/:-)
    - 3: followup with "thank you." or "noted" or "I'll try to do better next time"
    - 4: follow up with "would you like me to try again?" retriggering the bot after sending it the feedback

## Scraping (for data)

Projects that don't yet have good open data sources, so you'll have to build a scraper or do some ETL and combining of existing data sources. Many APIs like twitter limit search queries, so you'll have to fool them or scrape them from human-facing HTML.

- **job matcher**: scrape job descriptions
    - work through a scraping tutorial:
        - indeed: https://proxyscrape.com/blog/web-scraping-for-job-postings-using-python/
        - indeed: https://www.geeksforgeeks.org/scraping-indeed-job-data-using-python/
        - glass door: https://webscreenscrapingservices.medium.com/how-to-scrape-glassdoor-job-data-using-python-lxml-4db3b1da643f
    - build a web crawler that searches for careers pages
        - https://towardsdatascience.com/build-a-scalable-web-crawler-with-selenium-and-pyhton-9c0c23e3ebe5
    - NLP: extract named entities for technologies to rank popularity among employers
    - NLP: identify prosocial busineses (open-source, "doughnut economy", transparent job descriptions, regenerative business practices)
    - NLP: ML to learn which domain-specific terms don't translate well with google translate or DeepL
- **captcha smart**: build an agent smart enough to get past some easy captchas that prevent bots from interacting with websites
  - be clever about button clicks at x,y locations within buttons or checkboxes (don't click in the center with your AI)
- scrape quiz sizes for facts and Cyc-like general knowledge as well as quiz-bot material
  - history: https://www.currentgk.com/category/history/
  - technology/philosophy/industry: https://www.currentgk.com/
- **nonprofit sentiment**: Twitter ad scraper targeting nonprofits (hardcoded list of nonprofit account @handles, hashtags, keywords). simple sentiment analysis to determine favorability rating and trends and geography -- [How to scrape tweets from Twitter](https://towardsdatascience.com/how-to-scrape-tweets-from-twitter-59287e20f0f1)


### Software Development and DevOps

Anything you can do to support open source, open data projects is very prosocial.
Here are some ideas:

- **django-agile**
    - project management in django app with table views and markdown import for easy task estimation
    - data science or NLP for predicting task difficulty (points) and completion dates
- **hacktoberfest**
    - https://www.youtube.com/channel/UC7HcWhSetq6nTlpMXPHKz_A
    - follow and support Chris Thompson at Portland Python Pirates (he'll give you FOSS contribution ideas)
- **MLOPS - GitLab project template - python package**
    - read docs on creating a template: https://gitlab.com/gitlab-org/project-templates/contributing
    - create a python project template using cookiecutter or pyscaffold, but with **NO** package dependencies
    - add dependency on pytest and an example unittest & doctest
    - automate unittests
    - automate doctests
    - add dependency on flake8 and add automated linting
    - postcommit hook with flake8 checking and warnings
    - add dependency on githooks? for
- **DEVOPS - GitLab Project Template - Django app**
    - read docs on creating a template: https://gitlab.com/gitlab-org/project-templates/contributing
    - create a python project template using cookiecutter or pyscaffold, but with **NO** package dependencies or the python package template above
    - add django dependency
    - enable admin interface
    - CICD auto-deployment to DigitalOcean, Linode, and/or HonestHost, etc (Azure, GCP, & AWS OK too)
- **MLOPs - automate Anaconda install**
    - Your goal is to automate the installation of Anaconda similar to this intern exercise that shows how to do it manually: [tangibleai/team/exercises/2-plan-your-prosocial-ai-project/](https://gitlab.com/tangibleai/team/-/blob/master/exercises/3-mlops-install-anaconda-multiple-users/README.md)
    - 1: use the python requests package and some simple NLP to write a function that can find the latest version of Anaconda3 from this url: https://repo.anaconda.com/archive/
    - 2: add a keyword argument for the function for the operating system (`Linux`, `MacOSX`, `Windows`)
    - 3: add a keyword argument for 64-bit (`x86_64` or `x86`)
    - 4: write a python function that uses requests to download the installer file from Anaconda's website just like `wget` or `curl` would do (whose url your function in 3 should return)
    - 5: execute the installer in "batch" mode (`-b`) on people's systems using python, here's a [stack overflow answer](https://stackoverflow.com/a/92395/623735) with some good ways to do that
- **nutrition advisor**
    - combat nutrition misinformation and advertisement with a database based on science
    - download the FDA nutrition database
    - find external data sources on nutrition to cross reference with the FDA
    - build a REST API to your database
    - build NLP that processes a recipe and estimates the nutrition of a recipe
    - build a chatbot (Rasa, qary, or custom flask app) that uses a nutrition API to answer user questions and create a dinner menu, diet plan, grocery list, or meal plan
- **automate internship**
    - 1: tanbot emailer.py and gmailer.py
    - 2: auto responder for applicants
    - 3: auto grader/accepter of applicants
- **baserow** - open source airtable (nocode cloud database)
    - 1: fork, star, clone, & install baserow: https://gitlab.com/bramw/baserow
    - 2: create a knowledge base or database of your favorite songs, books, ML datasets, learning resources, chatbots, FOSS mobile apps, python packages
    - 3: review the `baserow issues`
- **generate a flowchart from multiple-linked list**
    - give our in-house project, ConvoScript, an export-to Botpress or Rasa option (see dialog.py)
    - create a visual representation of dialog designs based on the structure (graphviz)
    - goal would be to generate a visual representation of a dialog tree (i.e. locations of the nodes) based on the dialog structure
    - if it were easily editable that would be amazing (you'd have a competitor for Botpress, RasaX, etc)
- **contribute to Rasa**
    * find a bug and fix it (github.com/rasahq/rasa/issues)
    * add or improve an information extraction NLP feature
    * add or improve an intent recognition feature (using a better transformer)
    * add a gui to help manage or edit rasa configuration files within your chatbot project
- **contribute to [Cookiecutter](https://github.com/cookiecutter/cookiecutter)**
    * cookiecutter uses the gh:accountname/reponame shorthand for github, make it work for gl:... (gitlab.com)
- create your own pypi package to do something you find useful
    * automate some boring stuff in your life or work
    * scripts to read your email inbox and summarize & prioritize them
    * script to find news
    * a note taking app that works offline
    * a django app for composing long documents or writing a novel or building a conversation graph (chatbot content)
-

### Django, Weba, Society, Culture, News

- **prosocial videos** (provid.io)
    - Django blog app totorial by Corey
    - deploy to digital ocean on videos.proai.org or videos.totalgood.org provid.io proai.io vid.eo vid.eos vid.org or similar
    - Text file upload capability for admins only (superusers)
    - Video file upload capability for admins (superusers)
    - email verification (SendGrid account for TangibleAI)
    - give trustworthy current and former interns admin accounts
    - create pro account for those that donate anything at all
    - give pro accounts access to upload pages but not admin
- **reclaim your content**
    - Download your Facebook data to json
    - Install Mastadon (Tusky) on your phone
    - Host a thetatoken CDN node: thetatoken.org
    - Move your python-related videos to pyvideo.org
    - Move your videos off YouTube to [lbry.tv/odysee.com](https://odysee.com) or [any decentralized video sharing service](https://blockonomi.com/youtube-alternative/) that doesn't exploit you
    - Learn about a Python or REST API to manage your video content on Digital Ocean Spaces, [lbry.tech](https://lbry.tech), or IPFS
- **wikipedia for news**
    - build a better Django app with better UX than https://en.wikipedia.org/wiki/Portal:Current_events
    - find news sources that are authoritative and have an API or are easily scrapable (1 day old news is still news)
    - find a wikipedia for news (scroll.com or Twitter Blue)
    - find news sites (like Twitter) with an open API
    - find an "authoritative" source like https://www.reuters.com
- **FOSS**
    - contribute to pyvideo.org software
    - contribute to zulip chat platform
    - contribute to fediverse [mastodon.social](https://mastodon.social/about) software packages
    - deploy a [mastodon.social](https://mastodon.social/about) server at home or in the cloud

### Machine Learning and Computer Vision

Some of these projects may be implemented without computer vision or machine learning and only require software development to implement and combine existing software (python modules).

- **automatic active learning**
    - 1: train image segmenter on a 10% subset of coco dataset or a self-driving car dataset
    - 2: find images/objects with lowest confidence
    - 3: predict object type, rotation, scale, and adjacent backgrounds that would bring confidence lower (gradient of confidence vs embedding componentsthen extrapolate to most difficult embedding
    - 4a: and find closest test image/objects to embedding or synthesize images with that extrapolated embedding (copy/paste or generative CNN))
    - 4b: rotate/scale/warp objects to add more difficult images to training set
    - 4c: alternatively find images in test set that are likely to have low confidence (similarity score of embedding)
    - 5: retrain
    - 6: return to 2 and repeat
    - 2: augment data by copy/pasting objects onto backgrounds from other images with random scaling/sizing/rotation
    - 3:
- **solve math problems with AI**
    - 1: https://projecteuler.net/ -- see if AI could help improve the solutions to any of these coding challenges
- **nudge phone-addicted politicians**
    - 1: read about this Flemish app: https://mashable.com/article/flemish-politicians-ai-phone-use
    - 2: see if you can find some similar open source software or object detectors you could use
    - 3: web app that ranks politicians according to how much time they spend not listening to legistlative debate
    - 3: stretch: obtain telephone numbers and e-mail addresses for legislators in your area and send the results to them directly
- **line drawing AI** - create SVG from raster image
    - 1: generate or compose random shapes and drawings of simple objects & scenes as line drawings in SVG or .dot or some other structured format for vector graphics
    - 1: alternatively find a database of raster images and their equivalent SVG files (line drawings)
    - 2: train an image segmentation model to create a binary raster image of the line drawing that fills in gaps and provides ensures that adjacent pixels are contiguously connected along any lines in the line drawing
    - 2: alternatively use a non-ML CV algorithm/filter for edge detection
    - 3: train an object detector to detect all the intersections of a line or arc and another line or arc.
    - 3: alternatively hard-code an algorithm that walks the graph of ocntiguous pixels to find intersections
    - 4: train a model to predict/infer the parameters of the straight line segments between intersections and classify it as an arc or line segment
    - 4: alternatively hard code an algorithm that converts node positions into straight line SVG parameters
    - 5: train a model to unify long line segments that pass through an intersection point to reduce the SVG complexity without changing it's accuracy.
    - 5: alternatively hard code an algorithm to do the same
    - 6: train a model to infer the parameters of the curved arcs in the SVG/raster image
    - 7: train an CV-NLG image captioning (video description) model to describe the SVG drawing in explicit detail
    - 8: train an NLP model to simplify/summarize the detailed line drawing description.
- **disability assistance**
    - 1: Play with https://fingerspelling.xyz/ to understand the state of the art in computer vision for assisting the deaf community. Also research TTS, STT, and NLP applications that assist people who are deaf.
    - 2: Research MyEye 2.0 and Orcam and other products for assisting blind and low-vision people.
    - 3: Research the cognitive assistance chatbots for cognitively impaired people and their caregivers
    - 3: See if you can come up with a small chatbot skill or DS model that might be useful for people who are blind, deaf, or cognitively impaired.
- **breed classifier**: manually create a classifier of dog breeds using height weight length and other measurements
    - one table (csv and excel tab) for each breed
    - practice statistics (mean, std, histogram) on folds of the data for 3 widely separated breeds
    - single variate classifier for the 4 breeds
    - manual thresholds based on midpoint between means
    - add "other" class and show how this can have implications at puppy kennels, dog breeds, etc
    - add "mix" class and show how this can have implications on dogs (euthanize  impure breeds, purebreds relegated to breeding kennels, inbreeding and genetic abnormalities, human society affected by the kinds of dogs they have as companions and their attitude towards other humans of mixed ancestry)
    - making a mistake about a Rottweiler could be fatal to a child buying that breed accidentally (just need to improve accuracy? or reduce false negatives on Rottweilerness)
- **tutor bot**: math tutor bot by Billy, but having trouble finding data
- **grade level bot**: grade essays 0-18 based on their grade level, by Josh
- **permit lottery prediction**: predict national park permit lottery wins by date, Winter 2021, by Winston
- **garbage classifier**: computer vision image classifier, object detector or image segmenter to identify categories of recyclable material (paper, glass,...), Winter 2021, by Billy, has a working classifier webapp
- **fish classifier**: Identify 1 of 4 species of trout with computer vision object detector on raspberry pi, Winter 2020
- **team beachcomber** - [trash tracker?] webapp to crowd source labeling of beach trash
    - 0: research similar apps and citizen science projects (one that tracked container ship spills, sneakers, etc)
    - 1: webapp to upload and store images (DRF CRUD)
    - 2: frontend to allow users to draw boxes around trash objects
    - 3: frontend to allow users to label trash from pulldown of nonexclusive tags
    - 4: frontend includes link to geolocation map of trash location
    - 5: webapp to allow human beachcombers to photolog the trash/treasure they pick up

## Deep Learning

- **ONNX**
    - implement a model in pytorch
    - convert it to ONNX: https://onnx.ai/
    - run the ONNX and pytorch models and compare for speed and accuracy on CPU
    - repeat comparison for GPU

### Robotics

- **beach comber bot** - robotic beach "sweeper", trash picker
    - 1: select a large RC car/truck kit with clearance suitable for beach navigation (big tires)
    - 2: select a robotic arm kit that can be mounted on top of the RC truck and reach the ground a few inches from each of the 4 tires
    - 3: select a (RC 2wd?) trailer with room for ~6L of trash (10 small plastic water bottles, 1000 cigarette butts)
    - 4: assemble the hardware
    - 5: add raspberry pi or NVidia Jetson to RC chassis with 360 deg dome camera on top of robot base
    - 6: implement self-driving "roomba" capability
    - 7: implement computer vision to identify trash and touch with robotic arm
    - 8: take photos of trash and upload to [baserow](https://baserow.io/) (see Baserow project under Software Dev)
- **littoral bathymetry bot** - small autonomous boat to survey shallow water harbors with sonar (fish finder)
    - 1: find NMEA-networked sonar
    - 2: find NMEA-networked GPS + IMU
    - 3: find raspberry pi accessory that can read/write NMEA
    - 4: Kalman filter for precise navigation (including kinematics to sea floor)
    - 5: simulate Kalman filter and sea state compensation, including latency and other error sources
    - 6: integrate it into this $4k RC boat: https://bathylogger.com/product/bathycat-survey-catamaran/
- Edit a [Wikipedia](en.wikipedia.org) article about prosocial AI:
    - [ethical AI](https://en.wikipedia.org/w/index.php?search=ethical+ai)
    - [prosocial AI](https://en.wikipedia.org/w/index.php?search=prosocial+artificial+intelligence)
    - [positive sum games](https://en.wikipedia.org/wiki/Win-win_game)
    - [regenerative business](https://en.wikipedia.org/wiki/Regenerative_economic_theory)
    - [AI control problem](https://en.wikipedia.org/wiki/AI_control_problem)
- advanced: **egamer**
    - follow [this tutorial](https://www.toptal.com/deep-learning/pytorch-reinforcement-learning-tutorial) AI to play FlappyBird
    - add keyboard commands for human to play the game
    - provide visual cues (flashing lights) to help human play the game
    - provide audible cues (tones) to assist a human playing the game
    - streaming video webapp in firefox: [tutorial](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Taking_still_photos)
    - robust cv: reduce quality of rendered video being fed to pytorch model during training and inference (starting from pretrained model)
    - use firefox on mobile phone to stream video of your game play to server (your laptop on the same local wifi)
    - align and warp image stream from webcam to match game being rendered on your laptop
    - computer vision transform to crop images to the trapezoidal video game window
    - warp and stabilize image stream to match rendered video on laptop
    - assist a human playing the game with voice commands
    - assist a human playing the game with conversational ai
    - apply system to website navigation tasks like wikipedia research, where "game" is to find answer to a specific question on wikipedia (stakeholder: Juan and other low vision people)
    - pitch blind assistant to aira.io ;)

### [qary](gitlab.com:tangibleai/qary)

- **search** command (wikipedia MVP, then maybe stack overflow, reddit, your personal emails/gmails/notes/docs)
    0. research elastic search solr and other full text search tools
    1. create semantic index of a directory of the paragraphs in the text files on your computer
    2. create reverse word index of a subset of wikipedia
- make qary teachable, "it would be better to say: xyz when I say that."
- **twitter search** from within qary? e.g. user: "tweets about Red Cross?" qary: "sentiment is 75% positive with a trending topic 'donate blood for fires in California'"
- **pulse** command (MVP DONE: Copeland Royall !!!!)
    0. give you the "pulse of the world" to connect you to the planet and society
    1. check out The New Paper and similar news summarizations services
    2. interview friends/family with example news summaries to craft summaries that engage them positively
    3. add qary skill to summarize the news of the planet ("pro pulse" prosocial pulse)
    4. add qary skill to poll user for their emotional well-being after reading your news pulse: positivity, anger, open mindedness, intelligence, sense of community
    5. webapp to allow people to subscribe to receive the pro pulse in their email
    6. add sms messaging delivery for the pulse
    7. allow people to donate

    - build classifer of news articles and RSS feeds for prosocial and positivenes
    - build function to download new articles that are prosocial
    - integrate into qary
- **question classifier**: use squad dataset to predict questions vs. non-questions, 90% accuracy on simple 2-gram 1000-word count vectorizer, Winter 2021, by Devi
- **question generator**: use SQuaD dataset to build question generator or spacy Matcher to create questions from statements
- **slang -> formal english**: also called style transfer

### Miscellaneous

- **legalese -> plain english**: translate legal terminology and complex statements to simpler language with a lower reading grade level, e.g. "subpoena" -> "document request"
- **sentence paraphraser**: Reword sentences so they say the same thing but in a different way (synonyms, reordering word). N-gram viewer database from Gutenberg would be good source of 5-grams to use.
- **yodafy**: Translate sentences from English to Yoda apostrophized order (subject-object-verb)
- **dialect/style transfer**: english -> Shakespearean,
- **named entity extraction** and identification from personal tweets (from list of small nonprofits)
- **bot detector**: trained on tweets from known bot and nonbot accounts?
- **sentiment analysis**: self-labeled data from Twitter based on hash tags
- **toxic comment detector**: winter 2021 by Uzi, Kaggle dataset
- **topic analysis**: for each nonprofit brand you find on Twitter, Reddit or social networks frequented by nonprofits
- **deflame**: style transfer using GPT-3 to turn toxic comments into nontoxic, helpful
- [Educational Data Nuggets](http://datanuggets.org/view-in-searchable-table/): beginner DS project ideas with very small data sets

### Past Cohorts Intern Projects

- **grouch**: Billy webapp for classifying recyclable trash
- **inspirebot**: Billy chatbot to recommend inspirational quotes
- **rapbot**: Uzi's rhyme suggester chatbot in qary
- **vaccirate**: Winston data science to predict childhood vax rates
- **nmt**: Hanna, Winnie, Sylvia machine translation for chapter 10 in _NLPIA 2nd Ed_, English -> French/Spanish/Amharic/Chinese, Winter 2021, by Winnie and Hanna
    - 1: improve spanish translation
    - 2: french translation
    - 3: bidirectional translation
    - 4: improved translation using more GRU layers
    - 5: improved translation using transformer + GRU decoder
    - 6: amharic dataset cleaning
    - 7: chinese tokenzer + translator?
    - 8: sections/chapter in nlpia
- **stateful quiz**: adaptive quiz that choses the best question based on previous answers, Winter 2021, by Jose
    - 1: `next` param str naming dest state within each state dict, if no `next` then proceed to next state in list (fall through)
    - 2: if `next` param not found then utter error message and proceed to random state
    - 3: `next` is list of dicts with `state` and `answer` as keys
    - 4: `answer` is list of answers (intent) which funnel to same next state
    - 5: next dict can contain `context` key (dict value) which triggers skill.context.update(context)
    - 6: `context` can contain strings with math expressions evaluated on the values during update, e.g. `{'score': '+=1'}`
    - 7: `context` can contain fstrs with variable substitution, e.g. `{'score': '= {happiness} + 1'}`
    - 8: `context` can contain python function name for function that takes previous_context and next_context as 2 mandatory arguments.
    - 9: `state` can have callable python function that takes context, next_context as arguments
    - 10: `state` can have dict that specifies conditionals on context
    - 11: information extraction from statements with fstring format in human answers
- **automatic mind map**: technical documents, [study aid for Jon](./reports/jon-wordmap.ipynb)
    - 1: play with the SpaCy examples in the documentation
    - 2: use spacy to break a text document into words
    - 3: for each word in the spacy Doc object, retrieve the `.vector` attribute (word embeddings)
    - 4: use PCA on all those word vectors to reduce the dimensionality to 2
    - 5: plot a scatterplot of all those 2D word vectors
    - 6: use plotly to create an interactive scatterplot so you can mouse over each word and display its string
    - 7: use clustering (K-Means) to assign an arbitrary cluster class label to each word
    - 8: plot a scatterplot with a different color for each word based on its cluster/class ID
- **inspiring quote**: qary skill to recommend an inspiring quote, Winter 2021, by Billy
    - 1: random quote suggester in `qary` skill
    - 2: content-based filter for unsupervised recommender
    - 3: collaborative filter for unupservised recommender
    - 4: user feedback mechanism: "Thank you!", "Perfect!", ":-)", ":(", "+1"
    - 5: reinforcement learning or "learning to rank" to improve recommendations
- **rap recommender**: chatbot to recommend a line of text that rhymes with the provide phrase, and maybe matches its rhythm Winter 2021, Uzi
    - 1: echo same sentence with last word replaced with rhyme
    - 2: echo same sentence with some synonym substitution and rhyme
    - 3: echo same sentence with slang substitution and rhyme
    - 4: generate multiple sentences with GPT-2 but filter those without rhyme
    - 5: and ness words to expand vocabulary of phone tokenizer and rhymer with soundex
    - 6: hard code additional destemmers for rhymer and phonifier
    - 7: phone expander/tokenizer for proper nouns or any sequence of characters
    - 8: deep learning RNN to generate phones for english words, including misspellings and slang and proper nouns (CMU phonifier/rhymer as training set)
    - 9: augment training set with proper noun lists and google n-gram viewer database
- **qa**: use BERT to answer questions based on a context paragraph, Summer 2020 by Olesya and Travis
- **summarize long tech documents**: Fall 2020 by Gary
- **elastic search** indexing and tuning for better open domain QA, Summer 2020 by Olesya

## NLP Research Projects
NLP research and tutorials could be incorporated into blog posts and NLPIA 2nd edition.
Anyone who contributes code or content or even ideas to NLPIA 2nd Ed will be listed as a contributing author, at a minimum.

- [ ] in ch3 or 4 on sentiment analysis & VADER, reading level estimators in python `textstat` package (Josh project)
- [ ] phone and soundex package that Uzi found
- [ ] rap rhyme project from Uzi
- [ ] linguistics of paraphrasing, moving words around according to their POS and transformations of the syntax tree
- [ ] anastrophe in yoda speak .3% of languages https://www.reddit.com/r/todayilearned/comments/l5unxd/til_yoda_of_the_star_wars_universe_uses_sentences/?utm_medium=android_app&utm_source=share
- [ ] mention semiotics
- [ ] mention linguistics and computational linguistics
- [ ] Mention number naming FSTP 6.3 when talking about named entity extraction
- [ ] mention chatspeak normalization FSTP 6.4 when talking about stemming and lematization

## Advanced ML/DL/AI Project Ideas

These crowdsourced data science platforms are a great source of ideas.
They can give you a head start on a difficult project by providing you with benchmark (state-of-the-art) code and data:

- [kaggle](kaggle.com)
- [papers with code](paperswithcode.com)

## Several projects where the prosoial AI projects could be mentioned

- [San Diego Tech Hub -- AI for Everyone]()
- [Prosocial Machine Learning (ProML)](https://gitlab.com/hobs/proml) for Packt Publishing
    - scheduled to start in March
    - Maria to lead
- [Data Science for Digital Health]() for UCSD
- [Data Science for Good](https://sandiegotechhub.com/data-science/) at San Diego Tech Hub
    - Hobson to copy material for UCSD DSDH course
- [NLPIA 2nd Ed](https://gitlab.com/hobs/nlpia-manuscript) for Manning.com
    - Maria - lead editor
    - Geoff - transformers and attention
    - Winnie (Ch 10-12) and anything RNN or translation
    - Sylvia (Ch 1-4) and anything Chinese
    - Hanna? Amharic? rare language translation
    - Gary - summarization


# 2021-01-14 Blog post GPT-3 and the AI Technological and economic singularity

Background: GPT-3 is 10 percent better at the "Zero-Shot" Penn Treebank word-level language model problem than BERT: https://paperswithcode.com/sota/language-modelling-on-penn-treebank-word

- GPT-3 stats
    - 48 transfomer layers
    - 175B learnable parameters (GPT-2 1.5B, BERT 0.4B)
    - pretrained on data unavailable to anyone but big tech
    - requires massive compute resources just to do inference (to **use** the model)
    - only the architecture is open source and it's not innovative (brute force, add layers)
- GPT-3 performance on benchmark tasks
    - GPT-3 wins on 3 out of 11 general language modeling tasks
    - GPT-2 also wins on 3 out of 11 tasks (and it requires 500x fewer parameters)
    - BERT

[GPT-3 (Generative Pretrained Transformer version 3)](https://en.wikipedia.org/wiki/GPT-3): a recurrent neural network or autoregressive model.

## backlog

[Django](https://www.educative.io/) tutorial at educative.io (by Mohammed)

## Ethics

- Hedonism
- Deontology
- Consequentialism
    - Utilitarianism
    - Pragmatics
    - Deweyanism

## Machine Learning
