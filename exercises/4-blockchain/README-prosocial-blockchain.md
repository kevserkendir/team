# Prosocial Blockchain

Blockchain and cryptocurrency are often associated with planet-baking energy consumption and cyber crime financing.
But blockchain technology can enable some prosocial applications as well.

What is Web 3.0?

- Web1 was web pages owned and controlled by corporations
- Web2 was social media data and applications controlled by corporations
- Web3 is the decentralization and peer-to-peer social media interactions

## Blockchain

Blockchain technology enables you to maintain a public ledger of any data you like.
The first and most well-known application is cryptocurrency where that ledger sheet serves exactly the same purpos as your bank's ledger of your financial transactions.
Currency and financial transactions went virtual years ago, with the advent of debit cards and SWIFT and other electgronic payment processing networks.
However, they must be centralized in order to maintain the integrity of the ledger.
When you make a purchase with a debit card, both you and the online store must trust your bank to dedecut the appropriate amount from your bank account and not let you spend money you don't have.
And the online store has to trust their own bank to accurately record that transaction in their ledger for their business.
Banks ou need to trust your bank to maintain your account balances as you transfer money around between accounts or pay vendors.

## Examples

- CoApp
- PoAuth - NFT for badge and on-chain record of an emotion/moment/memory with art/text to represent it
- POAP - Proof of Attendance protocol - instantiating memories who what when where
- [lens protocol](https://twitter.com/lensprotocol) - grassroots cooperation protocol
- disco.xyz - chose how much data they want to share with the world
