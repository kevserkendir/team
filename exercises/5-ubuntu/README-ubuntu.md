# Ubuntu tips and tricks

Here are somethings I needed to do to my Ubuntu installation to optimize my environment for software development.

In addition, here are some Linux setup guides you might want to check out:

* https://www.tecmint.com/initial-ubuntu-server-setup-guide/
*

## Security Holes

Ubuntu comes with a lot of bloatware that creates security vulnerabilities.
If you want Ubuntu to run on a server handling sensitive or personal data (including your laptop), you probably want to clean it up using some of the recommendations here:

https://www.tecmint.com/initial-ubuntu-server-setup-guide/

```bash
sudo apt update
sudo apt -y upgrade
sudo apt -y autoremove
sudo apt -y clean
```

Often you want a sudo user with a regular home directory:

```bash
sudo adduser me
sudo chage -d0 me
sudo usermod -a -G sudo me
````

Now log in as this new "me" user and create a password:

```bash
su - me
```

```bash
hostnamectl
```

```text
   Static hostname: hobson-framework
         Icon name: computer-laptop
           Chassis: laptop
        Machine ID: 0184974928894112abc779753ae9dc52
           Boot ID: ded0c8a2fee54ea9abe3e420374413ae
  Operating System: Ubuntu 20.04.3 LTS
            Kernel: Linux 5.13.0-30-generic
      Architecture: x86-64
```

For most servers you want to use short memorable, recognizable names.
I've seen people use the names of movies or superheros, but I like to have a list of [real world heroes](https://en.wikipedia.org/wiki/Lists_of_activists) or [technology pioneers](https://en.wikipedia.org/wiki/EFF_Pioneer_Award):

* [Assassinated Human Rights Activists](https://en.wikipedia.org/wiki/List_of_assassinated_human_rights_activists
, and I check them off as I create servers)
* [Lists of Activists]https://en.wikipedia.org/wiki/Lists_of_activists
* [EFF Pioneer Award Winners](https://en.wikipedia.org/wiki/EFF_Pioneer_Award#Winners)

- [x] musk
- [x] king
- [x] parks
- [ ] swartz


### Disable CUPS service

The cups service allows printers to automatically add themselves back whenever you add them.
The cafe where I work (Coffee N Talk downtown San Diego) was hacked and their printer kept connecting to my machine until I disabled CUPS
```bash
sudo systemctl stop cups-browsed
sudo systemctl disable cups-browsed

```

## Audio

When I updated the kernel my sound stopped working. Speakers were completely dead and when I turned up the volume on my headset they displayed the "Muted" icon on the desktop for my laptops.

I reinstalled alsa and pulseaudio and that seemed to reactivate the left speaker.
Then I installed PulseAudio Volume Control using the Ubuntu Software GUI.
With that I was able to "lock" the left and right balance, and that seemed to disable the System Settings for Sound until I rebooted.
Once I rebooted both speakers worked and my headset was at normal volume.

### References

- https://www.maketecheasier.com/fix-no-sound-issue-ubuntu/
- https://itsfoss.com/fix-sound-ubuntu-1304-quick-tip/


## Framework-specific (Intel chipsets)

### Intel GPU drivers

Check to see what kind of video card (GPU) you have:

```bash
lspci | grep VGA
```

#### _`hobs@framework-laptop output`_:
00:02.0 <span style="color:red">**VGA**</span> compatible controller: Intel Corporation Device 9a49 (rev 01)

Install the Intel repo PGP keys:

```bash
sudo echo && sudo apt-get install -y gpg-agent wget
wget -qO - https://repositories.intel.com/graphics/intel-graphics.key |   sudo apt-key add -
```

Install the GPU tool binary and source code (`-dev`) packages:

```bash
sudo apt-get update
# binaries
sudo apt-get install \
  intel-opencl-icd \
  intel-level-zero-gpu level-zero \
  intel-media-va-driver-non-free libmfx1
# dev tools
sudo apt-get install \
  libigc-dev \
  intel-igc-cm \
  libigdfcl-dev \
  libigfxcmrt-dev \
  level-zero-dev
```




Add your user to the "render" group to be able to run the GPU tools.

```bash
gpu_username=$(stat -c "%G" /dev/dri/render*)
sudo usermod -a -G $gpu_username $USER
```

For me that works to create the `render` group and add my `hobs` user to that `render` group:

```bash
groups $USER
```

#### _`hobs@framework-laptop output`_:
```text
hobs : hobs adm cdrom sudo dip plugdev input render lpadmin lxd sambashare docker
```


### Freezing (CPU Lock-Ups)

After an upgrade to Ubuntu 20.04 or later it may repeatedly lock up and fails to respond.
Try holding [CTRL] + [PRT_SCR] while typing [R] [E] [I] [S] [U] [B] (reboot even if system utterly broken).
But if it fails to respond to any keyboard or mouse input you may have an Intel process that is incompatible with recent linux kernels.
This causes the Intel processor to spontaneously go to sleep.

To fix this you can wait for the kernel fix (could take years) or disable the CPU sleeping feature with a kernel option in your boot program (Grub).
So you'll need to power down the old fashioned way (hold the power button down).
Then you can power your computer back on and quickly fix the grub boot options.
This [Ask Ubuntu](https://askubuntu.com/a/4412/31658) has all the details:

```bash
sudo sed -i s/quiet splash/quiet splash intel_idle.max_cstate=1/g /etc/default/grub
sudo update-grub
sudo shutdown
```

Next time you power it back on, all should be fine.


## Invisible USB Drives

Sometimes no matter how much you unplug and replug a USB drive from the USB port, Ubuntu fails to mount it with the `Files` application or detect it in the `Disks` application.
Even `ls /dev` won't list any `/dev/sda` or `/dev/sdb` drives for it.
The following commands seem to wake it up:

```bash
lsblk
lsusb
```

That should reveal `/dev/sda` and create an icon for it in the `Files` application.

## Battery life

You can reduce wear and tear on your Li Ion battery in any laptop by minimizing the time the battery is charged above 60% or discharges below 40%.
This means that if you leave your laptop plugged in all day and keep it charged to 100% its capacity will decline much faster than if you only let it charge to 50% or 60%.
Ubuntu can do this automatically for you if you set the maximum charge % in your Ubuntu system settings.
Unfortunately this setting is reset every time you boot your computer.
So you probably want to create a systemd process that resets this setting to your desired charge level each time you reboot.
And when you stop this service you can have it reset to 100%, say before you go on a roadtrip.

Here's the systemd file:

```bash
[Unit]
Description=Set the battery charge threshold
After=multi-user.target

[Service]
Type=oneshot
ExecStart=/bin/bash -c 'echo 60 > /sys/class/power_supply/BAT0/charge_control_end_threshold'
ExecStop=/bin/bash -c 'echo 100 > /sys/class/power_supply/BAT0/charge_control_end_threshold'

[Install]
WantedBy=multi-user.target
```

## Touchy Laptop Touchpad

If I don't have a mouse plugged in, the trackpad corrupts what I type nearly every time I pause. It happens in Sublime Text 3, nano, bash, and GMail, everywhere I use the keyboard. I can't keep my hands far enough away from the keyboard to get any work done at all without corrupting files, closing windows, sending unfinished e-mails, etc. I might as well have a corrupted keylogger and mouse hijacker (gremlin). As I type this, I'm going to have to g home to my KVM switch and mouse to finish this article. I just plugged in a mouse and that wasn't enough.

This has been a perpetual ever since I moved 100% to Ubuntu - each and every time I've ever installed Ubuntu on a Laptop. And it typically takes months for me to solve it.

The track pad driver settings are just too sensitive. My finger or palm can be almost half a cm above it and  it will register a weird drag/click that moves the cursor, selects and deletes text. Thank God for ctrl-Z.

This tool helped for a bit, until I upgraded Ubuntu and it overwrote my settings:

```bash
sudo apt install aptitude
sudo aptitude install gnome-tweaks gnome-tweak-tool
gnome-tweaks
```

After launching `gnome-tweaks` I then clicked on Keyboard & Mouse and scrolled down to "Mouse Click Emulation" at the bottom.
I left it turned on and confirmed that 2-finger right click was already turned on.
Also two-finger natural scrolling was already turned on.
Neither of these options were working until I did the next "combination" to unlock this feature.

0. Turn on "Show Extended Input Sources (increases the choice of input...)"
1. Turn on "enable additional input device options"
2. Plug in a mouse
3. Completely power down for 10 seconds.
4. Turn back on with mouse still plugged in.
5. Unplug mouse.

Turning on "Disable Touchpad while typing" does not immediately take effect.
Fortunately the , but this seems to have no affect anymore, after the upgrade.
Also, plugging in a mouse doesn't help either.


However if I plug in a mouse and reboot , everything seems fine. Even after I unplug the mouse and use the touchpad.
The touchpad now actually moves the cursor and does what I want without garbling everything I do.

Here are some other diagnostic commands you might try.

```bash
xinput list | grep -i touch
xinput list-props "PIXA3854:00 093A:0274 Touchpad" | grep -i hand
xinput list-props "PIXA3854:00 093A:0274 Touchpad" | grep -i sens
xinput list-props "PIXA3854:00 093A:0274 Touchpad"
```


## Headset not detected

You can't use the aux audio jack output with your 3.5 mm headset/headphones because it doesn't appear as an output device in Zoom Audio Settings or Ubuntu Sound Settings.

### 1. alsactl restore

```bash
sudo alsactl restore
sudo shutdown now
```

1. Open Settings -> Sound
2. Look for the the output pulldown that lists Builtin Speakers.
3. Insert and remove the headset jack plug several times watching the pulldown.

### 2. change pulseaudio conf

The `[Element Speaker]` needs to be turned on within `analog-output-headphones.conf`

#### _`/usr/share/pulseaudio/alsa-mixer/paths/analog-output-headphones.conf`_
```
cd /usr/share/pulseaudio/alsa-mixer/paths/
diff -c1 analog-output-headphones.conf.fixed analog-output-headphones.conf.bak
*** analog-output-headphones.conf.fixed	2022-02-14 14:35:38.102373917 -0800
--- analog-output-headphones.conf.bak	2022-02-14 14:33:00.125151674 -0800
***************
*** 119,122 ****
  [Element Speaker]
! switch = on
! volume = ignore

--- 119,122 ----
  [Element Speaker]
! switch = off
! volume = off
```

### 3. Try `alsactl restore` again

```bash
sudo alsactl restore
sudo shutdown now
```

1. Open Settings -> Sound
2. Look for the the output pulldown that lists Builtin Speakers.
3. Insert and remove the headset jack plug several times watching the pulldown.
