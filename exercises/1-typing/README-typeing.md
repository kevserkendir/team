# Typing

Communication is the most important skill for any team.
It's critical to our collective intelligence as a team.
Imagine if one of clusters of neurons in your brain wasn't firing well or had a lot of "noise".
Imagine what that would do to your intelligence and ability to think.
It would even affect your brain's prosocial instincts.
More intelligent people are more prosocial, because they have a nonzero sum game mindset.

Like English written communication, typing is one of the most important skills you can develop.
Typing and reading is how you communicate a lot of your thoughts in the modern world... both humans and machines.
Until voice recognition is fast and flawless, typing is the **only** way to communicate accurately with a machine.
And that's what Tangible AI is all about, communicating with machines.

In addition, if you are a developer who is passionate about your crafe, you can appreciate how import typing and reading is in your professional life.

So here are some fun games to quickly level-up your touch typing and even your reading skill:

https://games.sense-lang.org/EN.php
