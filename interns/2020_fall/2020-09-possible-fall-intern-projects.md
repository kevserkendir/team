# possible projects for interns

### Eda

- Twitter ad scraper targeting nonprofits (hardcoded list of nonprofit account @handles)
    - [How to scrape tweets from Twitter](https://towardsdatascience.com/how-to-scrape-tweets-from-twitter-59287e20f0f1)
- elasticcloud as repository for scraped tweets
- Tweet scraper targeting hard-coded list of nonprofit relevant hashtags
- named entity extraction and identification from personal tweets (from list of small nonprofits)
- bot and ad filtering/identificaiton
- sentiment analysis
- summarization of sentiment for each nonprofit brand
- topic analysis for each nonprofit brand
- twitter search from within qary? e.g. user: "tweets about Red Cross?" qary: "sentiment is 75% positive with a trending topic 'donate blood for fires in California'"

### John

- spelling corrector
- GED, GEC, GEH
- dialog planner to coach ESL learners in composing correct sentences (Mohammed's webapp)
- Drawing/listening ESL games that could be autmated with a chatbot:
    - [ESL Listening Games](2020-09-esl-writing-skill-coach-high-schoolers.md)
- Grammar correctors/suggester like Grammerly, but we'd like to be more conversational, like a parent or teacher
    - [Free Writing Assistant for Google Docs by ETS (SAT Test prep)](https://mentormywriting.org/)


### Vishal

- NLP within qary?
- context planner?
- actions/commands/reminders in qary?
