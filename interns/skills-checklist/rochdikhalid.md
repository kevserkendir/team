# Skills Checklist

You'll learn these skills over the 10 weeks, but not in this order.
Think of this as what to expect on your final exam.
You'll learn much more than this.
But these are the skills that I find most professional software development teams expect from a junior developer.
 to see if you have what it takes to contribute to a typical Python software development team.

* 01: Programming (Python):
    * [x] install Anaconda and use it to launch a Jupyter Notebook
    * [x] use `conda` to create environments and install packages in an environment
    * [x] install packages from pypi using `pip`
    * [x] importing modules within packages like sklearn and Pandas
    * [x] install packages from source code (--editable)
    * [ ] install and import packages from gitlab
    * [x] conditions: `if`, `else`
    * [x] loops: `for`, `while`
    * [x] functions (args, kwargs, `return`)
    * [x] classes: `class`, methods, attributes
    * [x] scalar numerical data types (`int`, `float`)
    * [x] sequence data types (`str`, `bytes`, `list`, `tuple`)
    * [ ] mapping (`dict`, `Counter`)
    * [x] sets (`set`)
    * [x] create stackoverflow account
    * [x] reading Tracebacks and error messages
    * [x] getting help (`help()`, `?`, `??`, `[TAB] completion`, `vars()`, `dir()`, `type()`, `print()`, `.__doc__`)
    *
* 02: Bash Shell (Terminal)
    * [ ] bash nlp (`wc`, `grep` [stanford.edu/class/cs124/lec/textprocessingboth.pdf])
    * [ ] navigate directories (`cd`, `pwd`, `ls -hal`, `/`, `~`, `*`, `$HOME`)
    * [x] manipulate files + directories (`mkdir ~/code`, `mv`, `cp`, `touch`)
    * [x] work with text files (`more`, `cat`, `nano`)
    * [ ] pipes and redirects (`|`, `>`, `>>`, `<`)
    * [ ] processes (`ps aux`, `fg`, `bg`, `&`)
    * [ ] conditions (`&&`, `||`)
    * [ ] tab-completion
    * [ ] comments and shabang (`# `, `#!`)
    * [ ] permissions (`chmod`, `chown`)
    * [ ] running shell commands (`source`, `.`, `eval`)
    * [ ] finding files: `find . -iname qary -size +1k`
    * [ ] ssh to remote server: `ssh intern@totalgood.org`
    * [x] set up ssh keys: `ssh-keygen`, `ssh-copy-id`
    * [ ] json, yaml, xml, html parsing: `jq`, `yq`, `xq`, `htmlq`
    *
* 03: Git
    * [x] create gitlab account
    * [x] find and fork a project on gitlab.com
    * [x] create project in gitlab.com
    * [x] edit a file in gitlab.com (README.md)
    * [x] add a file using gitlab.com GUI
    * [x] upload a file to gitlab.com with GUI
    * [x] ssh public key in gitlab
    * [x] clone a repository from the command line
    * [x] create a merge request in gitlab
    * [x] `git branch`
    * [x] `git merge`
    * [x] resolve merge conflicts (know how they happen too)
    * [x] habitually use the `status`, `add`, `commit -am`, `pull`, `push` workflow
    *
* 02: Data
    * [x] loops: `enumerate`, `tqdm`
    * [x] list comprehensions to transform features: `[x**2 for x in array]`
    * [x] conditional list comprehension: `[x**2 for x in array if x > 5]`
    * [x] load csv: `df = pd.read_csv()`,  `df = pd.read_csv(sep='\t')`
    * [ ] loading `json` and `yaml` data formats
    * [x] `DataFrame`s from HTML tables: `df = pd.read_html()`
    * [x] `DataFrame`s from CSV download links:
    * [ ] bigdata: `df = pd.read_csv(chunk_size=...)`
    * [ ] vectorized operations: `x1 + x2`
    * [x] concatenate: `pd.concat(axis=0/1)`
    * [x] `pd.Series.apply()`
    * [x] indexing `DataFrame`s with `.iloc` `.loc` and `[]`
    * [x] how to one-hot encode a categorical (discrete) variable
    * [ ] how to vectorize a multicategory (multihot) variable
    * [ ] json, yaml, xml, html parsing: `jq`, `yq`, `xq`, `htmlq`
    *
* 05: Statistics & Visualizations
    * [x] `np.random.rand`, `.randint`, `.randn`, `np.random.seed`
    * [ ] standard deviation
    * [x] error metrics (RMSE, loss)
    * [ ] optimization (objective function, gradient descent)
    * [ ] [68-95-99.7 Rule](https://en.wikipedia.org/wiki/68%E2%80%9395%E2%80%9399.7_rule)
    * [ ] creating and interpreting histograms (transparent overlays of 2+ histograms)
    * [ ] probability distructions (normal, log/Poisson)
    * [ ] distribution skew
    * [x] how to create and interpret a scatterplot
    * [ ] how to create a loglog scaled scatterplot
    * [ ] how to create and interpret a scatter matrix plot
    * [ ] data scedasticity and stationarity
    *
* 06: Data Science
    * [ ] conventional approach to DS: 1. Hypothesis, 2. ETL, 3. EDA, 4. Modeling, 5. Validation & Testing, 6. Explanation (insights)
    * [ ] Agile DS approach: 1. engineer one feature, 2. model, 3. validate, 4. return to step 1 adding an addition feature, ...
    * [ ] at least 6 kinds of features: 2 fundamental, 4+ "special" information-rich data types
    * [x] how to select a target variable
    * [ ] 4 kinds of data science problems and example applications for each
    * [ ] choosing a model type: [sklearn decision flow chart](https://scikit-learn.org/stable/tutorial/machine_learning_map/index.html)
    * [ ] create polynomial features with sklearn
    * [x] train, predict and evaluate `LinearRegression` & `LogisticRegression` models
    * [ ] example probems that `LinearRegression` & `LogisticRegression` are good for
    * [ ] your "go-to" models for regression and classification: Lasso & LogisticRegression
    * [x] make predictions/estimates with pretrained machine learning model
    * [x] training/fitting a machine learning model on a training set
    * [x] validating/evaluating/testing a model on a validation set or test set
    * [x] overfitting: how to detect it, and what to do about it
    * [ ] class bias: how to detect it, and what to do about it
    * [ ] model performance metrics: `R**2 score`, RMSE, F1-score, accuracy, precision, recall
    * [ ] how is time series forecasting different from modeling tabular data
    * [ ] ARIMA and how to implement it in python or using sklearn's LinearRegression
    * [ ] confusion matrix
    * [ ] residual plots and how to use them to improve your model (using heteroscedasticity)
    * [ ] what kinds of problems are neural networks good for?
    * [ ] what are CNNs for? (example applications)
    * [ ] what are RNNs (LSTM, GRU, Transformers) for? (example applications)
    * [ ] what is BERT, GPT-2
    *
* 07: Webdev & Comptuer Networks
    * [ ] python `requests` package and `.get`, `.post` methods and `stream=True`
    * [x] `pandas.read_html()` function
    * [ ] python `BeautifulSoup4` package
    * [ ] address bar in browser for HTTP GET requests with arguments
    * [ ] address bar in browser for HTTPS requests with arguments
    * [ ] `wget` or `curl` for command line GET requests with arguments
    * [ ] `wget` or `curl` for command line POST requests with data (arguments)
    *
* 08: Software development Best practices
    * [ ] zen of python
    * [ ] style guides, linters, PEP8
    * [ ] Syntax highlighters
    * [ ] Anaconda auto-formatter for Sublime
    * [ ] self-explanatory variable names (verbs for functions, plural nouns for lists, etc)
    * [ ] [Napolean-style docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)
    * [ ] [doctests](https://django-testing-docs.readthedocs.io/en/latest/basic_doctests.html)
    * [ ] [testing](https://python-102.readthedocs.io/en/latest/testing.html) (`pytest`, TDD, `unittest`, regression
    * [ ] how to use git on a team
    * [ ] constructive [code reviews](https://youtu.be/iNG1a--SIlk)
    * [ ] patterns and [antipatterns](http://docs.quantifiedcode.com/python-anti-patterns/)
    tests)
    *
* 09: NLP and chatbots:
    * [ ] 4+ fast & easy numerical (scalar) features to extract from NL text
    * [x] extract bag of words vectors from text
    * [ ] extract TFIDF vectors from text
    * [ ] PCA on TFIDF vectors
    * [ ] TSNE on PCA (LSA) vectors from TFIDF vectors
    * [ ] at least 4 chatbot approaches: rules, search, grounding, generative models
    * [ ] create a zero-order markhov chain (model) of text
    * [ ] first and second order markhov chain models (Uzi's diagrams)
    * [ ] information content (entropy) of a string
    * [ ] algorithmic (Kolmogoroph) complexity of a string (and how to estimate it)
    * [ ] chatbot architectures (deterministic, generrative, knowledge-based)
    * [ ] techniques for intent recognition: exact match, keywords, doc vectors, BERT/USE embeddings
    * [ ] keyword intent recogition
    * [ ] information extraction, entities
    * [ ] efficient full text search (indexing)
    * [ ] utterances, turns, dialog trees, dialog cycles, dialog engines
    * [ ] conversation management, conversation design
    * [ ] voice recognition and multimodal UI (AllenAI)
    * [ ] a common way to use transfer learning for any NLP problem
    *
* 10: AI Businesses
    * [ ] agile (standups, retros, planning, poker)
    * [x] presentation and communication in English
    * [ ] stakeholder & user interviews, target variable(s), objective function(s)
    * [ ] introduction to Tangible AI
    * [ ] "How nonprofits use AI" webinar
    * [ ] open source project communities [notes](https://gitlab.com/tangibleai/team/-/blob/master/blog/draft/2021-10-21%20open%20source%20community%20best%20practices.md) [draft blog](https://gitlab.com/tangibleai/team/-/blob/master/blog/draft/Blog%20Post%20--%20The%20risks%20and%20rewards%20of%20open-source.md) [blog](https://proai.org/2021/04/26/)
    * [ ] differences between open source licenses: Hippocratic, MIT, GNU, Apache, CC-BYA
    * [ ] data privacy ethics and law (GDPR, HIPPA)
    * [ ] Common chatbot/NLP applications in the nonprofit world
    * [ ] Common chatbot/NLP applications in the for-profit world
    * [ ] The Technological Singularity
    * [ ] The Economic Singularity
    * [ ] Open source licenses and how to choose the right one
    * [x] What is AI?
    * [ ] Beneficial AI
    * [ ] The control problem
    * [ ] The Technological Singularity
    * [ ] The Economic Singularity
    * [ ] Job search
    * [ ] career planning, values, reputation, and ethics
    *
* 11: Databases
    * [x] connect to and query a SQL database
    * [x] query data from multiple tables in SQL
    * [x] add or remove data in SQL database
    * [x] create a sqlite database (peewee, sqlalchemy, Django)
    * [x] add a table to a sqlite database
    * [x] add a field (text, int, bool, float)
    * [x] add a foreign key to another table
    * [ ] add a foreign key back to the same table (graph db)
    *
* 12: Computer Vision
    * [ ] 4+ kinds of image processing problems (classification, segmentation, obj detection, counting, captioning)
    * [ ] 2+ video processing problems (activity recognition, movie description)
    * [ ] 3+ simple, effective numerical (scalar) features to extract from an image
    * [ ] how to do PCA on a collection of images for feature reduction
    *
