# Skills Checklists

There are several versions of the skills checklist scattered about.
The official up-to-date version is here in [blank.md](./blank-skills-checklist.md)

## Skills Checklists Introductions (Exercise Descriptions)

tangibleai/team/exercises/tanbot-messages/intern-challenge-01-skills-checklist.md
tangibleai/team/exercises/tanbot-messages/intern-challenge-10-skills-checklist-self-evaluation-details.md 
tangibleai/team/exercises/tanbot-messages/intern-challenge-10-skills-checklist-self-evaluation.md
tangibleai/team/exercises/tanbot-messages/intern-challenge-01-skills-checklist.md
tangibleai/team/exercises/tanbot-messages/intern-challenge-10-skills-checklist-self-evaluation-details.md

## Python Programming Checklist with Quiz

tangibleai/team/exercises/2-python/python-skills-checklist.md

## Original (2020) version of the Skills Checklist

tangibleai/team/interns/2020-intern-syllabus-and-skills-checklist.md
