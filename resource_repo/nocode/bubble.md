## Analytics with Segment 
Connecting Segment plugin: https://manual.bubble.io/bubble-made-plugins/segment

## APIs
API reference: https://bubble.io/reference#API.get_api.create_thing

## Making Mobile Apps
One-page apps: https://forum.bubble.io/t/architecture-layout-best-practice-one-page-with-multiple-functions-groups-vs-multiple-pages/12541/8

### Misc Hacks
Selecting/unselecting several buttons in group: https://www.youtube.com/watch?v=ImJausGZLs4&t=611s

### Push Notifications
Getting OneSignal PlayerID from Nativator: https://forum.bubble.io/t/nativator-io-launch-its-preview-app-test-and-fine-tune-your-app-before-conversion/69769/159

## Useful Plugins & examples 
Reorderable list: https://forum.bubble.io/t/reorder-items-in-list-of-things-of-current-user/52495

List shifter to iterate over Bubble lists: 
https://forum.bubble.io/t/list-shifter-reverse-rotate-swap-and-iterate-loop-over-bubble-lists-now-at-v1-4-adds-numeric-option-get-index-action/56237/111


Deploying an app: 
- On Android - very straightforward; 
- On iOS - you need a App Store Connect developer account 

Google sheet functions: 
= substitute(SUBSTITUTE(LOWER(A1), " ","_"), "#","")

Slicing the components:
=TRANSPOSE(OFFSET('Copy of Interventions List'!$G$25,'Import Helper Grid'!$A2,'Import Helper Grid'!$B2,4,1))

Adding names to the components:
==TRANSPOSE(OFFSET('Copy of Interventions List'!$E$25,-24,'Import Helper Grid (Service)'!$B2,1,1)) 


Lessons Learned: 
- Floating Groups are not scrollable! 
- Index page and how to change it: https://forum.bubble.io/t/what-is-index-page-function/82428/2
